# Behat Docker Image

An image suitable for running Behat tests.

## Why?

You might wonder, there are already many similar images out there, so why bother with another one?
The reason is simple, the others tend to fall in two categories: either they do too much or they do too little.
Considering none of them seem to be actively in use, I decided to have my own.

Hopefully, the folks at Behat might eventually roll out an official image.

## Components
- git, curl, wget (in case you need to clone or download your sources)
- PHP, with extensions:
  `ext-curl`,
  `ext-iconv`,
  `ext-json`,
  `ext-mcrypt`,
  `ext-pdo`,
  `ext-pdo_mysql`,
  `ext-sockets`,
  `ext-xdebug` (disabled by default, see below),
  `ext-xml`,
  `ext-zip`
- Composer
  - Prestissimo (installs Composer dependencies in parallel)
  - Behat
  - Mink, with drivers: `Zombie`, `Goutte`, `Selenium2`
  - PHPUnit 5 or 6 (depending on PHP version)

## Enabling XDebug

For performance reasons, XDebug is disabled by default.
It can be enabled globally by setting environment variable `ENABLE_XDEBUG` to `"1"`.

Alternatively, you can also do it from shell:
```bash
setxdebug on    # enable xdebug
setxdebug off   # disable xdebug
```

## Caching Composer

_work in progress_