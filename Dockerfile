ARG BASE_IMAGE
FROM $BASE_IMAGE

# Copy scripts
COPY docker/ /
RUN chmod +x /usr/local/bin/setxdebug

# Configure PHP
RUN echo "memory_limit=-1" > "$PHP_INI_DIR/conf.d/memory-limit.ini" \
 && echo "date.timezone=${PHP_TIMEZONE:-UTC}" > "$PHP_INI_DIR/conf.d/date_timezone.ini"

# Update apk repositories
RUN echo "http://dl-3.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories \
 && echo "http://dl-3.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
 && echo "http://dl-3.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

# Install PHP extensions
ENV ENABLE_XDEBUG "0"
RUN apk --no-cache --update add \
        git \
        apk-tools \
        libmcrypt-dev \
        libmcrypt \
        curl-dev \
        curl \
        wget \
        libxml2-dev \
        pcre-dev \
        ${PHPIZE_DEPS} \
 && pecl install xdebug \
 && docker-php-ext-install \
        curl \
        iconv \
        json \
        mcrypt \
        pdo \
        pdo_mysql \
        sockets \
        xml \
        zip \
 && docker-php-ext-enable xdebug \
 && apk del \
        pcre-dev \
        ${PHPIZE_DEPS} \
 && if [ "$ENABLE_XDEBUG" == "1" ]; then ENABLE_XDEBUG="on"; else ENABLE_XDEBUG="off"; fi \
 && setxdebug $ENABLE_XDEBUG

# Set up Composer and dependencies
ENV COMPOSER_ALLOW_SUPERUSER "1"
ENV COMPOSER_HOME "/.composer"
ENV PATH "/.composer/vendor/bin:${PATH}"
RUN wget "https://composer.github.io/installer.sig" -O - -q | tr -d '\n' > installer.sig \
 && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
 && php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
 && php composer-setup.php \
 && php -r "unlink('composer-setup.php'); unlink('installer.sig');" \
 && chmod +x composer.phar \
 && mv composer.phar /usr/local/bin/composer \
 && composer global install --working-dir=/.composer --ansi --no-progress

# Minimize size
RUN apk del --purge --force linux-headers binutils-gold gnupg zlib-dev libc-utils \
 && rm -rf \
        /var/lib/apt/lists/* \
        /var/cache/apk/* \
        /usr/share/man \
        /tmp/*

WORKDIR /app
